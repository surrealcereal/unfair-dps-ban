import requests
import json
import Levenshtein

from weapon_vanity import vanity_dict


class Weapon:
    def __init__(self, _name, _min, _max, _category):
        self.name = _name
        self.min = _min
        self.max = _max
        self.category = _category


def get_weapons():
    url = "https://raw.githubusercontent.com/AdKats/AdKats/master/adkatsblweaponstats.json"

    try:
        weapon_stats = json.loads(requests.get(url).text)["BF4"]
    except:
        raise RuntimeError(
            "There was a problem obtaining the allowed DPS stat limits from the AdKats GitHub page.\nThere may be no available internet connection.")

    weapons = list()
    for category, weapons_of_category in weapon_stats.items():
        for weapon_name, dmg in weapons_of_category.items():
            weapon = Weapon(weapon_name, dmg["min"], dmg["max"], category)
            weapons.append(weapon)
    return weapons


def get_user_weapon(weapons):
    def get_approx(weapon, weapon_names):
        approx = list()
        for name in weapon_names:
            if Levenshtein.distance(weapon, name) <= 2:
                approx.append(name)
        return approx

    def get_input():
        return input("Please enter the weapon you want to query: ").lower()

    user_weapon = get_input()
    weapon_names = [weapon.name for weapon in weapons]
    # Allow the entry of both JSON names or names specified in vanity_dict.
    # To be able to query case-insensitively, vanity_dict needs to contain lowercase vanity entries.
    lowered_vanity = {k: v.lower() for k, v in vanity_dict.items()}
    while user_weapon not in weapon_names:
        if user_weapon in lowered_vanity.values():
            # Reverse the dictionary to obtain JSON format from user_weapon.
            reverse_dict = {v: k for k, v in lowered_vanity.items()}
            return reverse_dict[user_weapon]

        print("Please enter a correct weapon name.")
        expanded_weapon_names = weapon_names + list(vanity_dict.values())
        approx = get_approx(user_weapon, expanded_weapon_names)
        if approx:
            approx = "\n".join(approx)
            print(f"Perhaps you meant: \n{approx}")
        user_weapon = get_input()
    return user_weapon


def get_user_weapon_display(user_weapon):
    return vanity_dict[user_weapon]


def get_stats(user_weapon_display, adKats_weapon):
    def get_input(stat, user_weapon_display, extra):
        if extra:
            # So that the space does not have to be included in the argument.
            extra = f" {extra}"
        return float(input(f"Please enter your {stat} for the {user_weapon_display}{extra}: "))

    def perform(stat, warn, user_weapon_display, condition, extra=str()):
        value = get_input(stat, user_weapon_display, extra)
        while not condition(value):
            stat_capit = stat if stat.isupper() else stat.capitalize()
            print(f"{stat_capit} must be {warn}. Please enter a correct {stat} stat.")
            value = get_input(stat, user_weapon)
        return value

    kills = int(perform("number of kills", "above 0",
                user_weapon_display, lambda x: x > 0))
    check_exemptions(user_weapon_display, adKats_weapon,
                     check_kills=True, kills=kills)

    dps = perform("DPS", "above 0", user_weapon_display, lambda x: x > 0)
    hskr = perform("HSKR", "between 0 and 100", user_weapon_display,
                   lambda x: 0 < x < 100, extra="(in %)")/100

    return dps, hskr, kills


def check_exemptions(user_weapon_display, adKats_weapon, check_kills=False, kills=0):
    max_dmg = adKats_weapon.max
    if max_dmg > 50:
        output_displayer(
            f"be exempt from autobans with the {user_weapon_display} as the {user_weapon_display}'s maximum damage is above 50, with {max_dmg}.")

    if check_kills and kills < 50:
        output_displayer(
            f"be exempt from autobans with the {user_weapon_display}, as you have less than 50 kills on it, with {kills}.")


def get_equivalent_weapon(weapons, name):
    for weapon in weapons:
        if weapon.name == name:
            return weapon
    raise RuntimeError(f"user_weapon {name} not matched.")

    adKats_weapon = find_equivalent_weapon(weapons, name)


def check_stats(user_weapon_display, dps, hskr, kills, adKats_weapon):
    modifiers = list()
    max_dmg = adKats_weapon.max

    expected_dmg = max_dmg * (1 + hskr)
    perc_diff = (dps - expected_dmg) / expected_dmg

    # Modify this to 0.6 if you are playing on a server with below 65% maximum health.
    trigger_level = 0.5

    if kills < 100:
        trigger_level *= 1.8
        modifiers.append(
            f"The plugin will be 80% more tolerant due to having less than 100 kills with the {user_weapon_display}.")

    if adKats_weapon.category == "handguns":
        trigger_level *= 1.5
        modifiers.append(
            f"The plugin will be 50% more tolerant due the {user_weapon_display} being a handgun.")

    if perc_diff > trigger_level:
        above = 100 * ((perc_diff/trigger_level) - 1)
        return f"be banned since you are {above:.2f}% above the accepted limit.", modifiers

    below = 100 * (1 - (perc_diff/trigger_level))
    return f"not be banned, as you are {below:.2f}% below the ban threshold.", modifiers


def output_displayer(result, modifiers=list()):
    print(f"You will {result}")
    if modifiers:
        modifiers = "\n".join(modifiers)
        print(
            f"\nThis result has been reached with the following modifiers:\n{modifiers}")
    # This can also be called with kills and weapon maximum damage, so allow it to end the script
    # prematurely. This'll have no effect on normal terminations.
    exit(0)


def main():
    weapons = get_weapons()
    user_weapon = get_user_weapon(weapons)
    user_weapon_display = get_user_weapon_display(user_weapon)
    adKats_weapon = get_equivalent_weapon(weapons, user_weapon)
    exempt_result = check_exemptions(user_weapon_display, adKats_weapon)
    dps, hskr, kills = get_stats(user_weapon_display, adKats_weapon)
    result, modifiers = check_stats(
        user_weapon_display, dps, hskr, kills, adKats_weapon)
    output_displayer(result, modifiers=modifiers)


if __name__ == "__main__":
    main()
