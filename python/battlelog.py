import requests
import json

from dps_check import get_weapons, get_user_weapon_display, get_equivalent_weapon


class WeaponStat:
    def __init__(self, _weapon_id, _kills, _hskr, _dps):
        self.id = _weapon_id
        self.kills = int(_kills)
        self.hskr = float(_hskr)/100
        self.dps = float(_dps)


def get_player_id():
    def get_input():
        inp_str = "Enter the URL to the Battlelog overview page of your soldier.\nIt should look like this: https://battlelog.battlefield.com/bf4/soldier/YOUR_NAME/stats/XXXXXXXXX/pc: "
        user_input = input(inp_str)
        digits = user_input.rstrip()[:-4][-10:]
        return int(digits)

    # I currently do not know what the range of these IDs are, so I am putting in no restrictions.
    # If this tool finds some use however, I'll go through and scan through every ID between 0 and
    # 10 000 000 to see what the boundaries are.
    while True:
        try:
            return get_input()
        except ValueError:
            print("Enter a valid URL.")


def get_stats(player_id):
    def normalize_id(i):
        normalized_dict = {
            "l115": "l96a1",
            "gol-magnum": "gol magnum"
        }
        if i in normalized_dict.keys():
            return normalized_dict[i]
        return i

    # json_s = requests.post(f"https://bf4db.com/api/player/stats/{player_id}").text
    json_s = requests.get(
        f"https://battlelog.battlefield.com/bf4/warsawWeaponsPopulateStats/{player_id}/1/stats/").text
    stats_dict = json.loads(json_s)["data"]["mainWeaponStats"]
    stats = list()
    for weapon in stats_dict:
        weapon_id = weapon["slug"]
        normalized_id = normalize_id(weapon_id)
        try:
            get_user_weapon_display(normalized_id)
        except KeyError:  # means not weapon irrelevant for AdKats, like c4-explosive
            continue
        kills = weapon["kills"]
        try:
            hskr = weapon["headshots"] / kills
        except ZeroDivisionError:
            hskr = 0
        try:
            # taken from AdKats, line 49453, same commit as in README
            dps = kills / weapon["shotsHit"] * 100
        except ZeroDivisionError:
            dps = 0
        weaponstat = WeaponStat(normalized_id, kills, hskr, dps)
        stats.append(weaponstat)
    return stats


def get_exempt(adKats_weapon, kills):
    max_dmg = adKats_weapon.max
    returns = list()
    if max_dmg > 50:
        returns.append("high max damage")
    if kills < 50:
        returns.append("insufficient number of kills")
    return returns


def check_stat_bfdb(dps, hskr, kills, adKats_weapon):
    max_dmg = adKats_weapon.max

    expected_dmg = max_dmg * (1 + hskr)
    perc_diff = (dps - expected_dmg) / expected_dmg

    # Modify this to 0.6 if you are playing on a server with below 65% maximum health.
    trigger_level = 0.5

    if kills < 100:
        trigger_level *= 1.8

    if adKats_weapon.category == "handguns":
        trigger_level *= 1.5

    if perc_diff > trigger_level:
        above = 100 * ((perc_diff/trigger_level) - 1)
        return above, True

    below = 100 * (1 - (perc_diff/trigger_level))
    return below, False


def get_ban(w, weapons):
    adKats_weapon = get_equivalent_weapon(weapons, w.id)
    exempt_result = get_exempt(adKats_weapon, w.kills)
    display_name = get_user_weapon_display(w.id)
    if exempt_result:
        return display_name, "exempt", " and ".join(exempt_result)
    results, banstate = check_stat_bfdb(w.dps, w.hskr, w.kills, adKats_weapon)
    return display_name, "banned" if banstate else "safe", results


def color_text(categories, rows):
    color = {
        "banned":   "\033[91m",  # red
        "safe":     "\033[92m",  # green
        "exempt":   "\033[96m"  # cyan
    }
    end = '\033[0m'

    colored = list()
    for category, row in zip(categories, rows):
        colored.append(f"{color[category]}{''.join(row)}{end}")
    return colored


def get_text(names, categories, results):
    def make_dict(*values):
        return dict(zip(["banned", "safe", "exempt"], values))

    full_text = make_dict("above limit by", "below limit by", "due to")
    result_format = make_dict(
        lambda x: f"{x:.2f}%.", lambda x: f"{x:.2f}%.", lambda x: x + ".")
    sort_method = make_dict(lambda x: -x[3], lambda x: x[3], None)

    categoric_rows = make_dict(list(), list(), list())
    for name, category, result in zip(names, categories, results):
        categoric_rows[category].append(
            (name, category, full_text[category] + " " + result_format[category](result), result))  # result at the end used for sorting

    for category, v in categoric_rows.items():
        categoric_rows[category] = sorted(v, key=sort_method[category])

    rows = list()
    for category, categoric_rows in categoric_rows.items():
        for row in categoric_rows:
            # append category for coloring, as the previous category order is obsolete after sorting
            rows.append((row[:-1], category))
    return rows


def align_text(rows):
    cols = list(map(list, zip(*rows)))
    widths = list()
    for col in cols:
        widths.append(max((len(i) for i in col)) + 2)

    for col, width in zip(cols, widths):
        for i, element in enumerate(col):
            col[i] = element.ljust(width)

    return list(zip(*cols))


def outputter(stats, weapons):
    output = list()
    for w in stats:
        out = get_ban(w, weapons)
        if out:
            output.append(out)

    names, categories, results = zip(*output)
    sorted_text, categories = zip(*get_text(names, categories, results))
    aligned_text = align_text(sorted_text)
    colored_text = color_text(categories, aligned_text)

    for row in colored_text:
        print(row)


def main():
    weapons = get_weapons()
    player_id = get_player_id()
    stats = get_stats(player_id)
    outputter(stats, weapons)


if __name__ == "__main__":
    main()
