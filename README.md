# AdKats (Non-LIVE) DPS Stats Autoban Guide

Unfairly banned from an AdKats server due to having high DPS stats even though you are innocent? If so, you have come to the right place, as this guide and repository as a whole will:

1. [show how the banning system works](https://codeberg.org/surrealcereal/unfair-dps-ban#1-how-do-the-bans-work),
	- 1.1. [provide a Python script to check if your stats will get you banned](https://codeberg.org/surrealcereal/unfair-dps-ban#1-1-python-script-to-check-dps-bans-before-they-occur),
2. [show how one can get unbanned](https://codeberg.org/surrealcereal/unfair-dps-ban#2-how-can-i-get-unbanned).

But, before we start off with the recommendations, let's answer a few important basic questions from the get-go.

## Basic Q&A
**When something is written out in upper snakecase (LIKE_THIS) in the document, it is meant to be substituted with the appropriate thing, e.g. PLAYER_NAME is to be substituted by the player who is relevant.**

### What is AdKats?
AdKats is an administration tool for BF4 servers that handles many things, automatically banning players based on their stats being one of these. It is open source, meaning anyone can look at the code to see how it functions. This feature will be instrumental in explaining what goes on behind the scenes.

### How do I know if I have been banned by AdKats?
There are a multitude of ways of detecting whether a server uses AdKats or not, *other than, rather obviously, asking the admins*. These can be split into two groups:

#### Server Mechanics:
As mechanics can be implemented by different plugins, it is impossible to say with 100% confidence that these mechanics being implemented in a server means it is running AdKats. However, I'd say that even one of these systems being in place makes the server *very* likely (about 90%) to be running AdKats, as not many equivalently feature-rich administration plugins exist for BF4.

* If, when trying to re-join the server after being banned, you are "Kicked by an admin.", the server is most likely running AdKats.
* If the server you are banned on has a reputation system, it is more than likely running AdKats.
* If the server you are banned on has means for players to kick others using commands like `!votekick PLAYER_NAME` or `!voteban PLAYER_NAME`, it is most likely running AdKats.
* If the server has a nuking system, where after all the flags have been capped by a team on conquest, they are all killed and prevented from respawning to prevent basecamping, it is more than likely running AdKats.

#### Server Messages:
As sentence formulations are specific to a program, one can be 100% confident in the usage of AdKats by a server if they have encountered these messages *exactly*.

* If, when you were being banned, you saw the message:
>Thank you for making our system look good. Goodbye. 

the server is running AdKats.

* If, when being reported, you have seen the message:
>Your reputation has automatically contested PLAYER\_NAME's report against you. 

the server is running AdKats.

* If you have ever encountered the peculiar message:
>You appear to be a victim of grenade cooking and will NOT be punished.

the server is running AdKats.

* If, when a nuke is active, the message:
>PLAYER\_NAME, the TEAM\_NAME nuke active for X seconds! 

is displayed when trying to spawn, the server is running AdKats.

### How do I know if I have been DPS autobanned?

When trying to join a server through Battlelog one can see their kick reason. To do that, you need to open the game through Origin (or EA, if you have that instead) and launch it from there, **even if you own the Steam version**. 

Then, go to Battlelog, log in, browse to the server browser tab and join the server you are banned from through finding it using the filter function. When being autokicked, a red banner will appear at the bottom of Battlelog, telling you the reason you are being kicked. If the ban reason looks like:
```
[4-WEAPONNAME-DPS-KILLS-HEADSHOTS-HITS]
```
then, you have been autobanned by AdKats due to your high DPS stats:

>DPS Checker [Code 4 Bans]
>	
>The damage per shot each player gets with their weapons is calculated from their battlelog stats, bans being issued if a player attains impossible damage. This section is now completely automated and enabled by default without a means to turn it off, any doubt about bans it issues should be taken with extreme caution as this system when properly configured has not issued a false positive ban in the 2 years it has been active on our servers.

<sub>From AdKats' [README](https://github.com/AdKats/AdKats/blob/master/README.md). We both know the last part is BS.</sub>

### Some other notes
The code taken is from 15th of December, 2022, from git SHA `270897b493dcaf598b3005bc5c70cedc4ea735b6`.

All line numbers being talked about pertain to the file `AdKats.cs` from the repo, unless specified otherwise.

##
With the preamble out of the way, we can actually start talking about our main topics.

## 1. How do the bans work?

### The Setup

When the plugin is enabled, the function `OnPluginEnable` is called, declared on line 9043.

This calls `PopulateWeaponStats` on line 9105, which is declared on line 62050.

This method calls `FetchWeaponDefinitions` on line 62055, which is declared on line 62094.

`FetchWeaponDefinitions` fetches the maximum allowable DPS stats for weapons as such (line 62103):
```cs
weaponInfo = Plugin.Util.ClientDownloadTimer(client, "https://raw.githubusercontent.com/AdKats/AdKats/master/adkatsblweaponstats.json" + "?cacherand=" + Environment.TickCount);
```

The mentioned `adkatsblweaponstats.json` is a JSON file containing said maximum *and minimum* allowable stats, e.g. (line 194 of `adkatsblweaponstats.json`):
```json
"qsz-92": {
	"max": 22,
	"min": 12.1
},
```
Using the returned hashtable from `FetchWeaponDefinitions`, `PopulateWeaponStats` populates the weapon statistics (lines 62063-62074).

`OnPluginEnable` later calls `InitThreads` on line 9175 (declared on line 10945), which calls `AntiCheatThreadLoop` on line 10995 (declared on line 17944), which calls `RunStatSiteHackCheck` on line 18013 (declared on line 18045), which calls `DamageHackCheck` on line 18059 (declared on line 18147).

### The Detection of Suspected Cheaters

Now, we get to the important bit.

First, only some categories of weapons are checked for damage hacks. These categories are declared on line 18195:
```cs
case GameVersionEnum.BF4:
	allowedCategories = new List<string> {
		"pdws",
		"assault_rifles",
		"carbines",
		"lmgs",
		"handguns"
	};
	break;
```
and later checked against on line 18226 (lines 18225-18226):
```cs
//Only count certain weapon categories
if (allowedCategories.Contains(weaponStat.Category))
```
Thus, if the weapon in question is a:
* shotgun,
* PDW,
* DMR,
* sniper rifle,
* gadget,
* grenade, or,
* knife,

it is exempt from investigation. Thus, the only battle pickup not exempt from investigation is the M60-ULT, a battle pickup on Operation Outbreak that counts as an LMG.

If the weapon has a maximum allowed damage **above 50**, stats pertaining to it are not investigated (line 18237):
```cs
//Only handle weapons that do < 50 max dps
if (weapon.DamageMax < 50)
```

The statistics based damage hack check is initiated on line 18308. The full code relating to this check is below, and will be dissected throughout the rest of this bullet point (lines 18308-18335):
```cs
if (weaponStat.Kills > 50)
{
	if (weaponStat.DPS > weapon.DamageMax && (!_UseHskChecker || weaponStat.HSKR < (_HskTriggerLevel / 100)))
	{
		//Account for hsk ratio with the weapon
		Double expectedDmg = weapon.DamageMax * (1 + weaponStat.HSKR);
		//Get the percentage over normal
		Double percDiff = (weaponStat.DPS - expectedDmg) / expectedDmg;
		Double triggerLevel = ((_soldierHealth > 65) ? (0.50) : (0.60));
		//Increase trigger level for kill counts under 100
		if (weaponStat.Kills < 100)
		{
			triggerLevel = triggerLevel * 1.8;
		}
		//Increase trigger level for sidearms
		if (isSidearm)
		{
			triggerLevel = triggerLevel * 1.5;
		}
		if (percDiff > triggerLevel && percDiff > actedPerc)
		{
			//Act on the weapon
			actedPerc = percDiff;
			actedWeapon = weaponStat;
		}
	}
}
```

The plugin first checks if the weapon in question has more than 50 kills recorded (line 18307):
```cs
//For full stat check only take weapons with more than 50 kills
if (weaponStat.Kills > 50)
```
If this number is not satisfied, the weapon in question will be exempt from DPS checks (and thus autobans).

It then checks if the DPS stat of the player is higher than the maximum allowed, and checks if the ratio of kills with a headshot (HSKR) is lower than 0.6 (the default of 60, declared on line 690, divided by 100) (line 18311):
```cs
if (weaponStat.DPS > weapon.DamageMax && (!_UseHskChecker || weaponStat.HSKR < (_HskTriggerLevel / 100)))
```
If the HSKR checker is disabled by the server admin, the HSKR part is disregarded. If the HSKR of the weapon is above 60%, the check is not run, presumably because the devs of AdKats thought people with so high of an headshot ratio would naturally have too high of a DPS to be called a cheater by the plugin.

Then, it calculates `expectedDmg`, which aims to create a DPS statistic that accounts for the HSKR (line 18314):
```cs
Double expectedDmg = weapon.DamageMax * (1 + weaponStat.HSKR);
```
`expectedDmg` is calculated rather arbitrarily: the maximum allowed damage of the gun is multiplied by the player's HSKR plus 1. So, for example, if I had 30 DPS on the AK-12 with a 50% HSKR, my `expectedDmg` for the AK-12 would be 45.

Then, a ratio between the freshly calculated `expectedDmg` and the actual DPS stat of the player is calculated, which is saved into `percDiff` (line 18316):
```cs
Double percDiff = (weaponStat.DPS - expectedDmg) / expectedDmg;
```
For the above example with the AK-12, the `percDiff` would be -0.333, which means we are off the hook for this weapon in terms of autobans.

The maximum `percDiff` allowed is calculated on line 18317, which constitutes `triggerLevel`:
```cs
Double triggerLevel = ((_soldierHealth > 65) ? (0.50) : (0.60));
```
If the server uses above 65% maximum health (which is the case on almost all servers as, by default, this is set to 100%), the `triggerLevel` becomes 0.5, and 0.6 if the server uses below 65% maximum health.

The `triggerLevel` is further modified by two things (lines 18318-18328):
```cs
//Increase trigger level for kill counts under 100
if (weaponStat.Kills < 100)
{
	triggerLevel = triggerLevel * 1.8;
}
//Increase trigger level for sidearms
if (isSidearm)
{
	triggerLevel = triggerLevel * 1.5;
}
```
If the weapon in question has less than 100 kills recorded, the `triggerLevel` is increased by 80%.

If the weapon is a handgun, the `triggerLevel` is increased by 50%.

The ban is carried out if `percDiff` is greater than `triggerLevel` and greater than -1 (by default, `actedPerc` declared on line 18222) (line 18328):
```cs
if (percDiff > triggerLevel && percDiff > actedPerc)
```

### The Carrying-Out of the Ban
If the checks above turn out to be correct, the `actedWeapon` is set to the currently investigated weapon (line 18332), which means `actedWeapon` is no longer `null` as declared on line 18221, thus the below check can pass and the ban can be initiated (line 18344):
```cs
if (actedWeapon != null)
```

If there is no ongoing round, the player is banned immediately (lines 18415-18434).

Otherwise, the plugin waits (line 1836) until the player is not online<sup>\*</sup>, has spawned once and 5 minutes have elapsed to begin processing the player's ban (line 18364):
```cs
while (banPlayer.player_online && !banPlayer.player_spawnedOnce && (UtcNow() - start).TotalSeconds < 300)
```

Once these conditions are met, the plugin waits a further 83 seconds, displays the text "Thank you for making our system look good. Goodbye." to the player 6 times, and waits for a final 7 seconds (lines 18375-18377):
```cs
Threading.Wait(TimeSpan.FromSeconds(83));
PlayerTellMessage(banPlayer.player_name, "Thank you for making our system look good. Goodbye.", true, 6);
Threading.Wait(TimeSpan.FromSeconds(7));
```

The player is banned afterwards (lines 18383-18400).

<sup>\*</sup>: The `player_online` attribute is set on joining the server. I am unsure to what it's doing here, as by looking at the name one would assume this being true would make the whole "shaming the cheater in front of the server by banning them during the match thing" pointless. 

## 1.1. Python Script to Check DPS Bans Before They Occur
If you want to check if you are safe from being banned, a Python script, called `dps_check.py`, is also included in the repository where you can input the name of the weapon in question, your HSKR and DPS with it, and see if you'd be banned by the default configuration of AdKats.

Or, alternatively, the other script in the folder called `battlelog.py` can be used to check through every weapon automatically by providing the script with a Battlelog profile URL.

To use `dps_check.py`, you need to have [python-Levenshtein](https://pypi.org/project/python-Levenshtein/) and [requests](https://pypi.org/project/requests/) installed.

For `battlelog.py`, [requests](https://pypi.org/project/requests/) suffices.

To install and use it, simply:
```
git clone https://codeberg.org/surrealcereal/unfair-dps-ban
cd unfair-dps-ban
cd python
```
and,
```
python dps_check.py
```
or,
```
python battelog.py
```
based on whichever you'd like to use. `dps_check.py` provides more detailed output, but is more tedious to use because of the manual input.

If the above made no sense to you, I'd recommend looking up:
* how one installs Python and pip,
* how one uses `pip` to install dependencies,
* how one installs git, and,
* how one runs Python scripts from the command line.

## 2. How can I get unbanned?

First of all, these autobans are permanent (line 18389). Thus, you'll have to ask the admins for an unban in all cases. If the admins are understanding and do their job correctly, they should provide you with a whitelist. However, unfortunately, this is not always the case. If this is also the case for you, you'll need to lower your stats before coming back and asking for an unban.

It is also important to note that DPS is calculated based on the damage dealt by shots that land, i.e. create a hitmarker, and thus shooting at the ground does not change this stat, only your accuracy.

Lowering your stats can be achieved in two ways:
	1. the free and non-efficient way, and
	2. the non-free but efficient way.

### 1. The Free and Non-Efficient Way

This is just playing the weapon due to which you are banned on another server to, hopefully, lower your stats. Make sure that the server does not use AdKats, as joining such a server will get you banned from there as well (if the admins have not tweaked or disabled anything). To ensure this, you can play on "Official" servers for a while, rather than "Ranked" ones, since those cannot enable plugins.

The caveat is that, if you are indeed innocent regarding this autoban, your normal play brought you to these stats. If that is the case, playing on another server may not make your stats go lower, and if you need to lower them drastically, this will make your job much harder.

You may try to play outside the intended ranges of the weapon, for example trying to snipe with a handgun, to obtain lower damage dealing hits. However, this is hard because:
* if the weapon is not intended to be used at that range, it is likely that its accuracy will also not be great at said range, making hitting actual shots harder,
and, perhaps more importantly,
* trying to play like this is just not fun.

These three factors combined probably means this method will not be efficient, however it still will cost you no money, unlike the other method.

### 2. The Non-Free and Efficient Way

This whole method relies on you getting another account to shoot at from a long distance. It also requires you have access to some sort of hardware macro, either on your keyboard or mouse.

To obtain the account, you'll probably have to pay up, unless you have another one already. You can go to a key reseller or account reseller to get the game for cheap, as it is quite old by now, or just buy it off Steam or EA when it is on sale, which happens quite frequently.

#### Buying the Game off Steam
If you are going to pay up and own the game on Steam on both accounts, do not try to purchase your second copy from the second account. This has the risk of getting the account flagged for fraud and your credit/bank card be banned of Steam. You'd be better off gifting it to the second account from your main account.

To be able to gift the game to your second account, you'll need to add them as a friend. However, as this account is new, it'll not have fulfilled the $5 spending limit to unlock community features, such as adding friends. 

If you do not want to purchase additional stuff to fulfill this quota, you can send a friend request from your main account. To do this, you need to:
* set a custom profile URL from the second account, and, 
* browse to said URL from the first account by using the browser in the Steam overlay screen (accessed through Shift+Tab while in-game by default) to send said request.

##
When you get access to a new EA account with BF4 on it, you need another PC to run the game on. This can either be achieved through an actual other piece of hardware, like a laptop, or a virtualized machine (VM).

#### Virtualization
VMs are basically computers inside of computers. So, if you do not have access to an actual other PC, you can make a program on your PC behave as one!

I'd recommend VMware, as VirtualBox is pseudo-virtualized, meaning it tries to cooperate with the main system to run better. This also means programs running under VirtualBox can more easily tell they are running in a VM, which PunkBuster could detect. I have not actually tried VirtualBox, but can confirm that VMware works for this method.

Set up a Windows VM with ~8GBs of memory and ~150GBs of storage. Look up how to do this if you do not know how to: search up "how to create windows vm vmware" on YouTube.

##
After setting up the VM or acquiring a second PC in hardware, download the platform you have the game on, whether that be Steam or EA, and BF4. Log into your other account. If you are using a VM, set all your graphics settings to low, as it will be nigh unplayable even with these settings.

#### Required Unlocks
Now, this method will have you make your second account stand somewhere far away but in view, and you'll use your actual account, where you need to lower your stats, to shoot them from far away, obtaining low damage hits.

This will require you have the ammo box unlocked on your main account, which should not be a very big problem, and have the medic bag unlocked on the second account.

You can either play to get the medic bag on the second account, or pay to get the Assault Shortcut Kit.

If you are going to pay to get the shortcut kit, check the "Buying the Game Off Steam" heading above for tips.

##
If you are going to play on it and are using a VM, log out of your main account on your main system, as the game will be quite unplayable on the VM.

#### Finding a Server to Perform the Method On
This method will also require you find a server where you can chill with your second account undisturbed. This server should:
* be 3200 tickets on Conquest with the time limit set to the maximum of 3 hours (will show up as 99:59), as we'll soon learn how to completely automate the shooting part, and you cannot make the setup (as in aligning both accounts in the correct positions etc.) perform itself automatically, so you need to get as much use out of one manual setup as possible,
* be lowly populated, so you'll have the least likelihood of being disturbed,
* have map(s) on rotation with open fields without much elevation differences, so you can actually see and shoot your other account from far away,
* be able to start the game with 2 or less people, so you can perform the method even if the server is empty.

I have found the server [GOLMUD WARS | Fast Vehichle *(sic.)* Respawn | 1 Player Start](https://battlelog.battlefield.com/bf4/servers/show/pc/799b552d-2b6e-495f-8105-870c3d24fa83/GOLMUD-WARS-Fast-Vehichle-Respawn-1-Player-Start/) to work quite well. 

You can use the rocks to the northeast of the A flag if you are playing Golmud Railway. Both teams can access that area and it is at the edge of the map, so not many people should come by, should they join the server:
![Said Rock](https://codeberg.org/surrealcereal/unfair-dps-ban/raw/branch/main/golmud-railway.png)
##

#### Automating the Process

Fully automatizing this process requires two parts:
1. automating the shooting, and,
2. bypassing getting kicked for being AFK on your second account.

#### 1. Bypassing the AFK Kick

This part is quite simple, as a short AutoHotkey script that makes you switch between your primary and secondary weapon should suffice. Trying to move slightly would disturb the fixed shooting position, so switching weapons is used. A sample AHK script is below:
```ahk
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

Loop {
	Send 1
	Sleep, 500
	Send 2
	Sleep, 500
}

F11::Pause
```
You can use this script by downloading [AutoHotkey](https://www.autohotkey.com), saving it into a file with the extension `.ahk` (you should be able to create a new AHK script through right clicking once you have downloaded AutoHotkey) and right clicking said file to `Run Script`. This script can be (un)paused by hitting F11.

<sub>*If you are on GNU/Linux and running through Proton, you should already know what you are doing. Using* [`pyautogui`](https://pypi.org/project/PyAutoGUI/) *could be an alternative.*</sub>

#### 2. Automating the Shooting
BF4 does not work very smoothly with inputs issued through AHK, so you'll notice the script from the first part switch your weapons very infrequently, even though it is supposed to do so every half a second.

This makes it impossible to use AHK for our shooting needs, so the inputs issued need to be the indistinguishable from normal inputs. This necessitate the use of hardware macros, such as the ones provided by Razer Cortex for Razer devices or Steelseries GG/Engine for Steelseries ones.

The macro should roughly look like this:
```
Right Click Down - 300ms delay - Left Click Down - 15ms delay - Left Click Up - 3000ms delay - Right Click Up
```

The delays are important due to the below reasons:
* the longest delay of 3000ms allows the second account to regenerate between shots. Make this delay not too short or else the second account will die due to not being able to regenerate fully, and do not make it too long or else you'll lose efficiency during the time you wait for nothing. I have found 3000ms to be a good number.
* the 300ms delay allows the gun to fully scope in so that you do not accidentally fire with hipfire accuracy as the shooter is zooming in.
* the 15ms delay is necessary for BF4 to recognize your keypresses properly.

Assign the macro to a key to start/stop looping when pressed.
##

### The Setup
Join the server and spawn as a support on your main account with the ammo box equipped, and as an assault on your second account with the medic bag equipped.

Quit your squads on both accounts so that no one can spawn on you.

Go to your desired location.

Throw down the medic bag from the second account. Throw it a bit further from your feet, as an inaccurate bullet could hit it if it's too close, resulting in the shooter killing the second account, as they are not unable to regenerate.

Run the AHK script. If in a VM you can tab out after running the script. The VM should take care of the rest.

Go far away enough from the second account so that the damage dealt by your weapon will be the least it can deal. You can check what the minimum for your weapon is on [sym.gg](https://sym.gg/index.html?game=bf4&page=charts) and at what distance that minimum damage is dealt. You can check in-game for the correct distance by shooting your second account when you think you are far enough and checking the damage dealt through the health remaining on your second account. Do not go unnecessarily far away though, as that'll make hitting shots harder due to the reduced accuracy.

After finding an optimal distance, throw down your ammo box to your side, so as not to shoot it, go prone to eliminate sway, aim on your second account, and run the macro.

Observe the system until a magazine is depleted: 
* Check to see if the second account can regenerate correctly, i.e. they are not slowly losing health over time. 
* Check to see if the majority of the bullets hit the target.
* Check to see if the main account replenishes their ammo after having gone through a magazine.
##

### Remote Monitoring
And optionally, stream your primary account to a new Twitch account on a stream without any categories, so that you can keep track of what's going on from your phone, without having to look at your computer. If you are to see yourself shooting at nothing (meaning your second account has been killed) or staring at the spawn screen (meaning your main account has been killed), you can then go to fix the situation.

## Final Words

Thank you for reading my guide. I hope it has been helpful. If you have any contributions/suggestions/questions etc. you can either:
* submit a pull request to edit this document through the [repository on Codeberg](https://codeberg.org/surrealcereal/unfair-dps-ban), or,
* contact me directly through `surrealcereal@tutamail.com`.

